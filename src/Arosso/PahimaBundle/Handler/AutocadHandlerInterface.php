<?php

namespace Arosso\PahimaBundle\Handler;

use Arosso\PahimaBundle\Entity\Floor;

interface AutocadHandlerInterface
{
    /**
     * Get Autocad elemet given the identifier
     *
     * @api
     *
     * @param string $name
     *
     * @return PageInterface
     */
    public function get($name);

    /**
     * Get the list of Autocad elemets.
     *
     * @return array
     */
    public function all();

    /**
     * Get the list of Autocad elemets for given floor
     *
     * @return array
     */
    public function allByFloor(Floor $floor);

}
