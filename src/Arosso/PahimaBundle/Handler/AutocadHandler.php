<?php

namespace Arosso\PahimaBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Arosso\PahimaBundle\Exception\InvalidFormException;

use Arosso\PahimaBundle\Entity\Floor;

class AutocadHandler implements AutocadHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get single Autocad element.
     *
     * @param string $name
     *
     * @return AutocadInterface
     */
    public function get($name)
    {
        return $this->repository->find($name);
    }

    /**
     * Get the list of Autocad elements.
     *
     * @return array
     */
    public function all()
    {
        return $this->repository->findBy([]);
    }

    /**
     * Get the list of Autocad elemets for given floor
     *
     * @return array
     */
    public function allByFloor(Floor $floor)
    {
        return $this->repository->findBy(['floor' => $floor]);
    }

}