<?php

namespace Arosso\PahimaBundle\Handler;

interface FloorHandlerInterface
{
    /**
     * Get Floor given the identifier
     *
     * @api
     *
     * @param string $name
     *
     * @return PageInterface
     */
    public function get($name);

    /**
     * Get the list of Floors.
     *
     * @return array
     */
    public function all();

}
