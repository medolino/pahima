<?php

namespace Arosso\PahimaBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\FormFactoryInterface;
use Arosso\PahimaBundle\Exception\InvalidFormException;

class FloorHandler implements FloorHandlerInterface
{
    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(ObjectManager $om, $entityClass, FormFactoryInterface $formFactory)
    {
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);
        $this->formFactory = $formFactory;
    }

    /**
     * Get single Floor.
     *
     * @param string $name
     *
     * @return FloorInterface
     */
    public function get($name)
    {
        return $this->repository->find($name);
    }

    /**
     * Get the list of Floors.
     *
     * @return array
     */
    public function all()
    {
        return $this->repository->findBy([]);
    }

}