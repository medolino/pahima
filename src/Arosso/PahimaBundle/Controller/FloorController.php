<?php

namespace Arosso\PahimaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Arosso\PahimaBundle\Exception\InvalidFormException;
use Arosso\PahimaBundle\Form\PageType;
use Arosso\PahimaBundle\Model\PageInterface;

class FloorController extends FOSRestController
{

	/**
	 * Get single Floor,
	 *
	 * @ApiDoc(
	 *   resource = true,
	 *   description = "Gets a Floor for a given name",
	 *   output = "Arosso\PahimaBundle\Entity\Floor",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when the floor is not found"
	 *   }
	 * )
	 *
	 * @param Request $request the request object
	 * @param string     $name      the floor name
	 *
	 * @return array
	 *
	 * @throws NotFoundHttpException when floor not exist
	 */
    public function getFloorAction($name)
    {
    	$floor = $this->getOr404($name);

        return $floor;
    }

    /**
     * List all floors.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @param Request     $request      the request object
     *
     * @return array
     */
    public function getFloorsAction(Request $request)
    {
        return $this->container->get('arosso_pahima.floor.handler')->all();
    }

    /**
     * Fetch Floor or throw an 404 Exception.
     *
     * @param string $name
     *
     * @return FloorInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($name)
    {
        if (!($floor = $this->container->get('arosso_pahima.floor.handler')->get($name))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$name));
        }

        return $floor;
    }

}
