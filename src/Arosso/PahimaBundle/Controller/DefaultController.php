<?php

namespace Arosso\PahimaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ArossoPahimaBundle:Default:index.html.twig', array('name' => $name));
    }
}
