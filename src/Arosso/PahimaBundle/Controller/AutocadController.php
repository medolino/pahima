<?php

namespace Arosso\PahimaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use JMS\Serializer\SerializationContext;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Arosso\PahimaBundle\Exception\InvalidFormException;
use Arosso\PahimaBundle\Form\PageType;
use Arosso\PahimaBundle\Model\PageInterface;

class AutocadController extends FOSRestController
{

	/**
	 * Get single Autocad element.
	 *
	 * @ApiDoc(
	 *   resource = true,
	 *   description = "Gets an Autocad Element for a given name",
	 *   output={
     *       "class"="Arosso\PahimaBundle\Entity\Autocad",
     *       "groups"={"element"}
     *     }, 
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when element is not found"
	 *   }
	 * )
	 *
     * @Annotations\View(serializerGroups={"element"})
     *
	 * @param Request $request the request object
	 * @param string     $name      elements name
	 *
	 * @return array
	 *
	 * @throws NotFoundHttpException when element not exists
	 */
    public function getElementAction($name)
    {
    	$floor = $this->getOr404($name);

        return $floor;
    }

    /**
     * List all Autocad elements.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\View(serializerGroups={"element"})
     *
     * @param Request     $request      the request object
     *
     * @return array
     */
    public function getElementsAction(Request $request)
    {
        return $this->container->get('arosso_pahima.autocad.handler')->all();
    }

    /**
     * List all Autocad elements for a given floor.
     *
     * @ApiDoc(
     *   resource = true,
     *   output={
     *       "class"="Arosso\PahimaBundle\Entity\Autocad",
     *       "groups"={"byFloor"}
     *     }, 
     *   statusCodes = {
     *     200 = "Returned when successful"
     *   }
     * )
     *
     * @Annotations\View(serializerGroups={"byFloor"})
     *
     * @param string     $floorname      floor name
     *
     * @return array
     */
    public function getElementsbyfloorAction($floorname)
    {
        if (!($floor = $this->container->get('arosso_pahima.floor.handler')->get($floorname))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$floorname));
        }

        return $this->container->get('arosso_pahima.autocad.handler')->allByFloor($floor);
    }

    /**
     * Fetch Floor or throw an 404 Exception.
     *
     * @param string $name
     *
     * @return FloorInterface
     *
     * @throws NotFoundHttpException
     */
    protected function getOr404($name)
    {
        if (!($element = $this->container->get('arosso_pahima.autocad.handler')->get($name))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$name));
        }

        return $element;
    }

}
