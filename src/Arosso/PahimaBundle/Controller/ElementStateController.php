<?php

namespace Arosso\PahimaBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Util\Codes;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Request\ParamFetcherInterface;

use JMS\Serializer\SerializationContext;

use Symfony\Component\Form\FormTypeInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Arosso\PahimaBundle\Form\PageType;
use Arosso\PahimaBundle\Model\PageInterface;

use Arosso\PahimaBundle\Plc\Element;

class ElementStateController extends FOSRestController
{

	/**
	 * Get a current state of a single Autocad element.
	 * NOTICE!!! Only for testing purposes (method is currently returning only random fake values - 0 or 1)
	 *
	 * @ApiDoc(
	 *   resource = true,
	 *   description = "Get a current state for single Autocad element",
	 *   output = "Arosso\PahimaBundle\Plc\Element",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when element is not found"
	 *   }
	 * )
	 *
	 * @param Request $request the request object
	 * @param string     $name      elements name
	 *
	 * @return array
	 *
	 * @throws NotFoundHttpException when element not exists
	 */
    public function getStateAction($name)
    {
    	$element = new Element();
    	$element->setName($name)
    		->setState(rand(0,1));

    	return $element;
    }

    /**
	 * Get current state of all Autocad elements for a given floor. 
	 * NOTICE!!! Only for testing purposes (method is currently returning only random fake values - 0 or 1)
	 *
	 * @ApiDoc(
	 *   resource = true,
	 *   description = " Get current state of all Autocad elements for a given floor.",
	 *   output = "Arosso\PahimaBundle\Plc\Element",
	 *   statusCodes = {
	 *     200 = "Returned when successful",
	 *     404 = "Returned when floor is not found"
	 *   }
	 * )
	 *
	 * @param Request $request the request object
	 * @param string     $name      elements name
	 *
	 * @return array
	 *
	 * @throws NotFoundHttpException when element not exists
	 */
    public function getStatesbyfloorAction($floorname)
    {
    	if (!($floor = $this->container->get('arosso_pahima.floor.handler')->get($floorname))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$floorname));
        }

        $elements = $this->container->get('arosso_pahima.autocad.handler')->allByFloor($floor);

        $i = 0;
        $returnStates = [];
        foreach ($elements as $el) {
        	$returnStates[$i] = new Element();
	    	$returnStates[$i]->setName($el->getName())
	    		->setState(rand(0,1));

	    	$i++;
        }

        return $returnStates;
    }

    /**
     * Update current state of a single Autocad element.
	 * NOTICE!!! Only for testing purposes 
     *
     * @ApiDoc(
     *   resource = true,
     *   description = "Update current state of a single Autocad element",
     *   input = "Arosso\PahimaBundle\Plc\Element",
     *   output = "Arosso\PahimaBundle\Plc\Element",
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the update has failed",
     *     404 = "Returned when element is not found"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return FormTypeInterface|View
     */
    public function postStateAction(Request $request, $name)
    {
    	$state = $request->request->get('state', null);
        if($state === null) {
        	throw new HttpException(400, 'Missing state value.');
        }

        $element = new Element();
    	$element->setName($name)
    		->setState($state);

        return $element;
    }
}