<?php

namespace Arosso\PahimaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Autocad
 *
 * @ORM\Table(name="autocad", indexes={@ORM\Index(name="floor", columns={"floor", "type"}), @ORM\Index(name="type", columns={"type"})})
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Autocad
{
    /**
     * @var float
     *
     * @ORM\Column(name="pos_x", type="float", precision=10, scale=0, nullable=false)
     * @Expose 
     * @Groups({"element", "byFloor"}) 
     */
    private $posX;

    /**
     * @var float
     *
     * @ORM\Column(name="pos_y", type="float", precision=10, scale=0, nullable=false)
     * @Expose 
     * @Groups({"element", "byFloor"}) 
     */
    private $posY;

    /**
     * @var string
     *
     * @ORM\Column(name="floor", type="string", length=45, nullable=false)
     * @Expose 
     * @Groups({"element"}) 
     */
    private $floor;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=false)
     * @Expose 
     * @Groups({"element", "byFloor"}) 
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Expose 
     * @Groups({"element", "byFloor"}) 
     */
    private $name;

    /**
     * Set posX
     *
     * @param float $posX
     * @return Autocad
     */
    public function setPosX($posX)
    {
        $this->posX = $posX;

        return $this;
    }

    /**
     * Get posX
     *
     * @return float 
     */
    public function getPosX()
    {
        return $this->posX;
    }

    /**
     * Set posY
     *
     * @param float $posY
     * @return Autocad
     */
    public function setPosY($posY)
    {
        $this->posY = $posY;

        return $this;
    }

    /**
     * Get posY
     *
     * @return float 
     */
    public function getPosY()
    {
        return $this->posY;
    }

    /**
     * Set floor
     *
     * @param string $floor
     * @return Autocad
     */
    public function setFloor($floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return string 
     */
    public function getFloor()
    {
        return $this->floor;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Autocad
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
