<?php

namespace Arosso\PahimaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Type
 *
 * @ORM\Table(name="type")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Type
{
    /**
     * @var string
     *
     * @ORM\Column(name="image1_path", type="string", length=100, nullable=true)
     * @Expose 
     * @Groups({"byFloor"}) 
     */
    private $image1Path;

    /**
     * @var string
     *
     * @ORM\Column(name="image2_path", type="string", length=100, nullable=true)
     * @Expose 
     * @Groups({"byFloor"}) 
     */
    private $image2Path;

    /**
     * @var \Arosso\PahimaBundle\Entity\Autocad
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Arosso\PahimaBundle\Entity\Autocad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="type")
     * })
     * @Expose 
     * @Groups({"byFloor", "element"}) 
     */
    private $type;



    /**
     * Set image1Path
     *
     * @param string $image1Path
     * @return Type
     */
    public function setImage1Path($image1Path)
    {
        $this->image1Path = $image1Path;

        return $this;
    }

    /**
     * Get image1Path
     *
     * @return string 
     */
    public function getImage1Path()
    {
        return $this->image1Path;
    }

    /**
     * Set image2Path
     *
     * @param string $image2Path
     * @return Type
     */
    public function setImage2Path($image2Path)
    {
        $this->image2Path = $image2Path;

        return $this;
    }

    /**
     * Get image2Path
     *
     * @return string 
     */
    public function getImage2Path()
    {
        return $this->image2Path;
    }

    /**
     * Set type
     *
     * @param \Arosso\PahimaBundle\Entity\Autocad $type
     * @return Type
     */
    public function setType(\Arosso\PahimaBundle\Entity\Autocad $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \Arosso\PahimaBundle\Entity\Autocad 
     */
    public function getType()
    {
        return $this->type;
    }
}
