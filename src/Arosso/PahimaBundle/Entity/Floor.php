<?php

namespace Arosso\PahimaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Floor
 *
 * @ORM\Table(name="floor")
 * @ORM\Entity
 * @ExclusionPolicy("all")
 */
class Floor
{
    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="string", length=100, nullable=false)
     * @Expose 
     * @Groups({"floor"}) 
     */
    private $imagePath;

    /**
     * @var float
     *
     * @ORM\Column(name="upper_right_x", type="float", precision=10, scale=0, nullable=false)
     */
    private $upperRightX;

    /**
     * @var float
     *
     * @ORM\Column(name="upper_right_y", type="float", precision=10, scale=0, nullable=false)
     */
    private $upperRightY;

    /**
     * @var float
     *
     * @ORM\Column(name="lower_left_x", type="float", precision=10, scale=0, nullable=false)
     */
    private $lowerLeftX;

    /**
     * @var float
     *
     * @ORM\Column(name="lower_left_y", type="float", precision=10, scale=0, nullable=false)
     */
    private $lowerLeftY;

    /**
     * @var \Arosso\PahimaBundle\Entity\Autocad
     *
     * @ORM\Id
     * @Expose 
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="Arosso\PahimaBundle\Entity\Autocad")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="floor", referencedColumnName="floor")
     * })
     * @Groups({"floor", "element"}) 
     */
    private $floor;



    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return Floor
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string 
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set upperRightX
     *
     * @param float $upperRightX
     * @return Floor
     */
    public function setUpperRightX($upperRightX)
    {
        $this->upperRightX = $upperRightX;

        return $this;
    }

    /**
     * Get upperRightX
     *
     * @return float 
     */
    public function getUpperRightX()
    {
        return $this->upperRightX;
    }

    /**
     * Set upperRightY
     *
     * @param float $upperRightY
     * @return Floor
     */
    public function setUpperRightY($upperRightY)
    {
        $this->upperRightY = $upperRightY;

        return $this;
    }

    /**
     * Get upperRightY
     *
     * @return float 
     */
    public function getUpperRightY()
    {
        return $this->upperRightY;
    }

    /**
     * Set lowerLeftX
     *
     * @param float $lowerLeftX
     * @return Floor
     */
    public function setLowerLeftX($lowerLeftX)
    {
        $this->lowerLeftX = $lowerLeftX;

        return $this;
    }

    /**
     * Get lowerLeftX
     *
     * @return float 
     */
    public function getLowerLeftX()
    {
        return $this->lowerLeftX;
    }

    /**
     * Set lowerLeftY
     *
     * @param float $lowerLeftY
     * @return Floor
     */
    public function setLowerLeftY($lowerLeftY)
    {
        $this->lowerLeftY = $lowerLeftY;

        return $this;
    }

    /**
     * Get lowerLeftY
     *
     * @return float 
     */
    public function getLowerLeftY()
    {
        return $this->lowerLeftY;
    }

    /**
     * Set floor
     *
     * @param \Arosso\PahimaBundle\Entity\Autocad $floor
     * @return Floor
     */
    public function setFloor(\Arosso\PahimaBundle\Entity\Autocad $floor)
    {
        $this->floor = $floor;

        return $this;
    }

    /**
     * Get floor
     *
     * @return \Arosso\PahimaBundle\Entity\Autocad 
     */
    public function getFloor()
    {
        return $this->floor;
    }
}
