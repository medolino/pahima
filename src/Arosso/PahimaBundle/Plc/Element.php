<?php

namespace Arosso\PahimaBundle\Plc;

use JMS\Serializer\Annotation as JMS;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Element
 * 
 * @ExclusionPolicy("all")
 */
class Element 
{
	/**
	 * @var string
	 * 
     * @JMS\SerializedName("name")
     * @JMS\Type("string")
     * @Expose 
	 */
	protected $name;

	/**
	 * @var int
     *
     * @JMS\SerializedName("state")
     * @JMS\Type("integer")
     * @Expose 
	 */
	protected $state;

    /**
     * Gets the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * Sets the value of name.
     *
     * @param string $name the name 
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of state.
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }
    
    /**
     * Sets the value of state.
     *
     * @param string $state the state 
     *
     * @return self
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }
}